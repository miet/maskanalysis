##
## WARNING THE FUNCTIONS SEEM NOT TO BE CONSISTENT WHEN THE MASK LABELS ARE DIFF NOT NUMBERED FROM 0 **WITH GAP 1**.

#' Returns the 3D boundaries of the pavoid englobing all voxels of value <index> in label volume <mask>  
#' @param mask a labeled (or binary) mask
#' @param index an integer representing the index of interest in the <mask>
#' @param extraBound an integer representing some extra margins to account for around the shape boundaries (can be positive or negative)
#' @return a list with two elements: 'cmin' : a 1d-array with the xmin,ymin,zmin coordinantes of and 'cmax' same but for xmax, ymax, zmax.
#' The function returns NULL if there is no voxel with value <index>.
getBounds <- function(mask,index=1,extraBound=0)
{
    indexXYZ = miet::miet_which(mask==index);

    if( !is.null(dim(indexXYZ)) )
    {
        bounds=list()
        res=c(range(indexXYZ[,1]),range(indexXYZ[,2]),range(indexXYZ[,3]))
        bounds[['cmin']] =res[seq(1,5,2)]+c(-extraBound,-extraBound,-extraBound)
        bounds[['cmax']] =res[seq(2,6,2)]+c(extraBound,extraBound,extraBound)
        return(bounds)
    }
    else{return(NULL)}
}

#' Returns the 3D maximal dimension of the space englobing all voxels of value <index> in label volume <mask>  
#' @param mask a labeled (or binary) mask
#' @param index an integer representing the index of interest in the <mask>
#' @return a 1-d vector with the nb of voxel in each dimension of the volume of iinterest 
#' The function returns NULL if there is no voxel with value <index>.
getExtend <- function(mask,index=1)
{
	indexXYZ = miet::miet_which(mask==index);

    if(!is.null(dim(indexXYZ)) )
    {
	extend = c((range(indexXYZ[,1])[2]-range(indexXYZ[,1])[1]),
			 (range(indexXYZ[,2])[2]-range(indexXYZ[,2])[1]),
			 (range(indexXYZ[,3])[2]-range(indexXYZ[,3])[1]));

    return(extend)
    }
    else{return(NULL)}
}

#' Returns the center of mass of a labeled (or binary) mask
#' @param mask a label (or binary) mask  
#' @param index the index of interest (in case of non-binary mask), default=1
#' @return the center of mass of the CC indexed <index> in <mask>. It returns null if the mask has no occurence of the <index>.
getCenterOfMass <- function(mask,index=1)
{
	indexXYZ = miet::miet_which(mask==index);

    if(!is.null(dim(indexXYZ)) )
    {
	centerofmass = c( mean(indexXYZ[,1]),mean(indexXYZ[,2]),mean(indexXYZ[,3]) ) 
    return(centerofmass)
    }
    else{return(NULL)}
}

#' Returns the list of unique non-zero elements of a given labeled mask
#' @param ccMap a labeled mask
#' @return an array of unique non-zero elements of a given labeled mask
nonZeroIds<-function(ccMap)
{
    return(setdiff(unique(c(ccMap)),0))
}

#' Returns a vector <res> such as res[<id>] equals 1 if there is a non zero
#' overlap btw the connected component labeled <id> in <refCC> (and 0 else) 
#' (i.e. there is 1 if the maps hit the corresponding CC)
#' @param hits a binary (or labeled) map 
#' @param refCC a labeled map (one id!=0 is one CC) to used to assess the map <hits>
#' @return a vector of integer <res>, such as res[ind]=1 <=>  <refCC>[ind] overlap <hits>.
getHitMap <- function(hits,refCC)
{
    res=c()
    for(i in nonZeroIds(refCC)) 
        {res[i]=ifelse(any(hits[refCC==i]!=0),1,0)}
    return(res)
}

#' Returns an array with the unique values of voxel in <label> corresponding to non-zero values in <mask>
#' Requires dim(label)==dim(mask)
#' @param label an array of label
#' @param mask an array of label
#' @return an array with the unique values of voxels in <label> corresponding to non-zero values in <mask>
CCId<-function(label,mask=label)
{
	if(any(dim(label)!=dim(mask))){stop('Error')}
	return(unique(label[mask!=0]))
}

#' Remove all CC in <img> so that their volume is lower than <lowestDimension>
#' @param img a volume of labels
#' @param dim the dimension of a voxel in mm3
#' @param lowestDimension the minimal volume to be kept in the 
#' @param relabel 
#' @return the volume of labels after having removed CC with volume lower than <lowestDimension> 
thresholdCCVolume<-function(img,dim,lowestDimension,relabel=T)
{
	minimalVoxelNb=lowestDimension/dim
	for(elt in setdiff(unique(c(img)),c(0)))
	{
		ind=which(img==elt)
		#print(paste0(length(ind),'/',minimalVoxelNb))
		if(length(ind)<minimalVoxelNb)
		{
			#print(paste0('Rm label ',elt))
			img[ind]=0
		}
	}
	if(relabel){img=oneGapLabel(img)}
	return(img)
}



#' Relabel a labeled volume so that label id starts at 1 and increases 1 by 1 (i.e. no gap)
#' @param img the labeled volume to relabel
#' @return a labeled volume so that label id starts at 1 and increases 1 by 1 (i.e. no gap)
oneGapLabel<-function(img)
{
	origLabel=setdiff(c(unique(img)),c(0))
	newLabel=seq(1,length(origLabel))
	res=img
	res[]=0
	for(elt in newLabel)
	{
		res[img==origLabel[elt]]=elt
	}
	
	return(res)
}

#' Returns TODO 
#' @param mask
#' @param index
#' @return ???
matchingId<-function(source, target)
{
	if(max(source)>0)
	{
	labelSource=setdiff(unique(c(source)),c(0))#seq(1,max(c(source)))
	
	res=list()
	for(elt in labelSource)
	{
		res[[elt]]=setdiff(unique(target[which(source==elt)]),c(0))
	}
	return(res)
	}
	else
	{return(list())}
}

#' Returns TRUE if the dimension of each argument passed as input as identical 
#' @param ... the set of arrays to compare
#' @return TRUE if the dimension of each argument passed as input as identical 
areDimensionsEqual<-function(...)
{
	dots <- list(...)
	if(length(dots)>=1)
	{d=dim(dots[[1]])}

	if(length(dots)>=2){
	for(elt in seq(2,length(dots)))
	{
		if(any(d!=dim(dots[[elt]]))){return(F)}
	}}

	return(T)
}


#' Returns a labeled volume whose all voxels from a given connected components are assigned to the same label
#' The labeled volume is build such as:
#' - the initialize volume is thresolded setting 0 to voxels with value upper than <threshold>
#' - the resulting volume is labeled from its CCs 
#' In practice, the function is just a wrapper to Cimg functions.
#' @param initialVolume  
#' @param threshold
#' @param highConnectivity if TRUE, use 2nd order connectivity if FALSE, use 1st order connectivity
#' @return an array with the same dimension than <initialVolume> and labeled according to the CC in <initialVolume>
getCC<-function(initialVolume,threshold=0.5,highConnectivity=TRUE)
{
  dim(initialVolume)=c(dim(initialVolume),1)
  les=imager::threshold(imager::cimg(initialVolume),threshold)
  res=imager::label(les,,high_connectivity=highConnectivity)

  res=res[,,]
  return(res)
}
